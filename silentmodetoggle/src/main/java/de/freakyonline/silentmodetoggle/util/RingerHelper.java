package de.freakyonline.silentmodetoggle.util;

import android.media.AudioManager;

public class RingerHelper {
    // privat, um zu verhindern, dass Benutzer ein RingerHelper-Objekt erzeugen
    private RingerHelper() {}

    /**
     * Stummgeschalteten Modus des Telefons umschalten
     */
    public static void performToggle(AudioManager audioManager) {
        // Wenn das Telefon stummgeschaltet ist, die Stummschaltung aufheben. Wenn
        // es normal ist, das Telefon stummschalten.
        audioManager.setRingerMode(
                isPhoneSilent(audioManager)
                        ? AudioManager.RINGER_MODE_NORMAL
                        : AudioManager.RINGER_MODE_SILENT);
    }

    /**
     * Gibt zurück, ob sich das Telefon im stummgeschalteten Modus befindet.
     */
    public static boolean isPhoneSilent(AudioManager audioManager) {
        return audioManager.getRingerMode()
                == AudioManager.RINGER_MODE_SILENT;
    }


}
