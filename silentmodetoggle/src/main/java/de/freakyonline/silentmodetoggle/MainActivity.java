package de.freakyonline.silentmodetoggle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.media.AudioManager;
import android.util.Log;
import de.freakyonline.silentmodetoggle.util.RingerHelper;


public class MainActivity extends AppCompatActivity {

    AudioManager audioManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

        setContentView(R.layout.activity_main);

        FrameLayout contentView = (FrameLayout) findViewById(R.id.content);

        contentView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RingerHelper.performToggle(audioManager);
                updateUi();
            }
        });

        Log.d("SilentModeApp","Dies ist ein Test");
    }

    private void updateUi() {

        ImageView imageView = (ImageView) findViewById(R.id.phone_icon);

        int phoneImage = RingerHelper.isPhoneSilent(audioManager) ? R.drawable.ringer_off : R.drawable.ringer_on;

        imageView.setImageResource(phoneImage);
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateUi();
    }

}
